import numpy as np
from vispy.color import Color

from component import PyOptiCADCanvas, Point
from component import Z_AXIS_DIRECTION
from component.detectors import RectangularScreen
from component.opticalPrimitives.convexParaboloid import Convex_Paraboloid
from component.sources import Ray, Ray_throughPoints

np.set_printoptions(precision=4, suppress=True, formatter={'float_kind': '{:0.2f}'.format})

canvas = PyOptiCADCanvas()

parabolicSurface1 = Convex_Paraboloid(1.5, 1.5, center=(0, 0, 0.), radius=0.5, name='P1', mediumBefore='Air',
                                      mediumAfter='BK7', color=Color((0.3, 0.3, 1), alpha=0.3), parentCanvas=canvas)
# parabolicSurface1.rotate_aboutY(30)
# parabolicSurface1.rotate_aboutX(20)
parabolicSurface2 = Convex_Paraboloid(1.5, 1.5, center=(1.2, 0, 0), radius=0.5, name='P2', mediumBefore='Air',
                                      mediumAfter='BK7', color=Color((0.3, 0.3, 1), alpha=0.3), parentCanvas=canvas)
parabolicSurface2.rotate_aboutY(90)

screen = RectangularScreen(Point(0.0, 0.0, 2.5), color=Color('green', alpha=0.9))

ray1Start = Point(-0.3, 0.2, 0.0, parentCanvas=canvas)
point_z = Point(-0.25, 0.2, 0.5, parentCanvas=canvas)
# point_x = Point(0.2, 0.2, 0.1, parentCanvas=canvas)

# ray1Direction = np.array((90, 90, 0))
ray1Start.show(color='g')
point_z.show(color='g')
# point_x.show(color='cyan')
ray2Start = Point(-0.4, -0.1, 0.2, parentCanvas=canvas)
ray2Start.show(color='y')
# ray2Direction = np.array((90, 90, 0))
ray2Direction = Z_AXIS_DIRECTION

incidentRay1 = Ray_throughPoints(ray1Start, point_z, name='R1', length=1.2, wavelength=0.35, color='green', dc=True,
                                 parentCanvas=canvas)
incidentRay2 = Ray(ray2Start, ray2Direction, name='R2', wavelength=0.85, color='yellow', parentCanvas=canvas)
# incidentRay3 = Ray_throughPoints(ray1Start, point_x, name='R3', color='cyan', parentCanvas=canvas)

# Reflection
r1 = incidentRay1.calculate_ReflectedRay(
	parabolicSurface1)  # .calculate_ReflectedRay(plane3).calculate_ReflectedRay(plane2)
r2 = incidentRay2.calculate_ReflectedRay(
	parabolicSurface1)  # .calculate_ReflectedRay(plane2)#.calculate_ReflectedRay(plane2)
# r3 = incidentRay3.calculate_ReflectedRay(parabolicSurface2)#.calculate_ReflectedRay(plane3).calculate_ReflectedRay(plane1)
#
# a,b=90, 90
# c=calculate_DirectionAngles(a,b)
# print(a,b,c)
# beam2 = CircularBeam(center=Point(0,-0.1,0,parentCanvas=canvas), beamDirection=(a,b,c), radius=0.3, noOfRays=64, wavelength=0.85, color='green', dc=False, parentCanvas=canvas)
# beam2.calculate_ReflectedBeam(plane1).calculate_ReflectedBeam(plane2).calculate_ReflectedBeam(plane3)

canvas.show()  # Display the PyOptiCAD canvas
