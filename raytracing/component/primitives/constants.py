import numpy as np

X_AXIS_DIRECTION = np.array((0, 90, 90))
Y_AXIS_DIRECTION = np.array((90, 0, 90))
Z_AXIS_DIRECTION = np.array((90, 90, 0))
X_AXIS_DC = np.array((1, 0, 0))
Y_AXIS_DC = np.array((0, 1, 0))
Z_AXIS_DC = np.array((0, 0, 1))

X_AXIS_NEG_DIRECTION = np.array((180, 90, 90))
Y_AXIS_NEG_DIRECTION = np.array((90, 180, 90))
Z_AXIS_NEG_DIRECTION = np.array((90, 90, 180))
X_AXIS_NEG_DC = np.array((-1, 0, 0))
Y_AXIS_NEG_DC = np.array((0, -1, 0))
Z_AXIS_NEG_DC = np.array((0, 0, -1))

ORIGIN = np.array((0, 0, 0))
